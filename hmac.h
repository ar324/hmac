#ifndef __HMAC_
#define __HMAC_

/* Computes and returns a pointer to the HMAC of 'msg' under 'key'. Returns NULL
 * on failure. Supported hash functions: SHA1, SHA256, and SHA512 (the latter
 * two from the SHA-2 family). The size of the HMAC (in bytes) is stored in the
 * location pointed by md_len. Free the allocated memory after use.
 */
unsigned char *
hmac (char *digest_name, unsigned char *key, int key_len, unsigned char *msg,
      int msg_len, int *md_len);

#endif
