#include <stdio.h>
#include "hmac.h"

int main ()
{
	char key[] = "key";
	char message[] = "The quick brown fox jumps over the lazy dog";
	unsigned char real[] = {0xde, 0x7c, 0x9b, 0x85, 0xb8, 0xb7, 0x8a, 0xa6,
		0xbc, 0x8a, 0x7a, 0x36, 0xf7, 0x0a, 0x90, 0x70, 0x1c, 0x9d, 0xb4, 0xd9
	};
	int md_len;
	unsigned char *md_value = hmac("SHA1", key, 3, message, 43, &md_len);
	for (int i = 0; i < md_len; i++) {
		if(md_value[i] != real[i]) {
			printf("failed at byte %d\n", i);
			return 0;
		}
	}
	printf("passed\n");
	return 0;
}
