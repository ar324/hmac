#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/evp.h>

/* Joins two 'unsigned char *'s. Free the allocated memory after use. */
static unsigned char *
concat_uns_char(unsigned char *s, int s_len, unsigned char *t, int t_len)
{
	unsigned char *result = (unsigned char *)
		malloc(sizeof(unsigned char) * (s_len + t_len));
	int i = 0;
	while(i < s_len) {
		result[i] = s[i];
		i++;
	}
	i = 0;
	while(i < t_len) {
		result[i + s_len] = t[i];
		i++;
	}
	return result;
}

/* Computes and stores the digest of 'msg' in md_value and its byte-length in
 * md_len.
 * Return codes:
 * 0 = successful
 * 1 = invalid digest name
 */
static int
get_digest(char *digest_name, unsigned char *msg, int msg_len,
	   unsigned char *md_value, int *md_len)
{
	const EVP_MD *md = EVP_get_digestbyname(digest_name);
	if(!md) {
		return 1; // unknown digest
	}

	EVP_MD_CTX *mdctx = EVP_MD_CTX_new();

	EVP_DigestInit_ex(mdctx, md, NULL);
	EVP_DigestUpdate(mdctx, msg, msg_len);
	EVP_DigestFinal_ex(mdctx, md_value, md_len);
	EVP_MD_CTX_free(mdctx);

	return 0;
}

/* Computes and returns a pointer to the HMAC of 'msg' under 'key'. Returns NULL
 * on failure. Supported hash functions: SHA1, SHA256, and SHA512 (the latter
 * two from the SHA-2 family). The size of the HMAC (in bytes) is stored in the
 * location pointed by md_len. Free the allocated memory after use.
 */
unsigned char *
hmac (char *digest_name, unsigned char *key, int key_len, unsigned char *msg,
      int msg_len, int *md_len)
{
	short BLOCK_SIZE;
	short DIGEST_SIZE;

	if(!strcmp(digest_name, "SHA1")) {
		BLOCK_SIZE = 64; // bytes
		DIGEST_SIZE = 20;
	} else if(!strcmp(digest_name, "SHA256")) {
		BLOCK_SIZE = 64;
		DIGEST_SIZE = 32;
	} else if(!strcmp(digest_name, "SHA512")) {
		BLOCK_SIZE = 128;
		DIGEST_SIZE = 64;
	} else {
		printf("Unsupported digest\n");
		return 0;
	}

	// Converting 'key' into the standard form.
	unsigned char key_padded[BLOCK_SIZE];
	int status;
	// Hashing key if it's too long.
	if(key_len > BLOCK_SIZE) {
		unsigned char reduced_key[DIGEST_SIZE];
		status = get_digest(digest_name, key, key_len, reduced_key,
				    md_len);
		if(status != 0) {
			printf("Error occurred within get_digest. Code: %d\n",
			       status);
			return 0;
		} else {
			// Padding with zeros on the right.
			for (int i = 0; i < BLOCK_SIZE; i++)
				key_padded[i] = (i < DIGEST_SIZE)?
						reduced_key[i]:0;
		}
	} else {
		// Padding with zeros on the right
		for (int i = 0; i < BLOCK_SIZE; i++)
			key_padded[i] = (i < key_len)?key[i]:0;
	}

	// Generating two keys, i_pad and o_pad, from key_padded.
	unsigned char o_pad[BLOCK_SIZE], i_pad[BLOCK_SIZE];
	for (int i = 0; i < BLOCK_SIZE; i++) {
		i_pad[i] = key_padded[i] ^ 0x36;
		o_pad[i] = key_padded[i] ^ 0x5c;
	}

	// Concatenating i_pad and msg; obtaining hash.
	unsigned char *inner_concat = concat_uns_char(i_pad, BLOCK_SIZE, msg,
						      msg_len);
	unsigned char *inner_res =
		(unsigned char *) malloc(DIGEST_SIZE * sizeof(unsigned char));
	if((status = get_digest(digest_name, inner_concat, BLOCK_SIZE + msg_len,
				inner_res, md_len)) != 0) {
		printf("Error occured within get_digest. Code: %d\n", status);
		return 0;
	}
	free(inner_concat);

	// Concatenating o_pad and inner_res; obtaining hmac (md_value).
	unsigned char *outer_concat = concat_uns_char(o_pad, BLOCK_SIZE, inner_res,
						      *md_len);
	free(inner_res);
	unsigned char *md_value =
		(unsigned char *) malloc(DIGEST_SIZE * sizeof(unsigned char));
	if((status = get_digest(digest_name, outer_concat, BLOCK_SIZE + *md_len,
					md_value, md_len)) != 0) {
		printf("Error occured within get_digest. Code: %d\n", status);
		return 0;
	}
	free(outer_concat);

	return md_value;
}
